object LibraryDependency {
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib:${LibraryVersion.KOTLIN}"
    const val CORE_KTX = "androidx.core:core-ktx:${LibraryVersion.CORE_KTX}"
    const val APP_COMPAT = "androidx.appcompat:appcompat:${LibraryVersion.APP_COMPAT}"
    const val MATERIAL = "com.google.android.material:material:${LibraryVersion.MATERIAL}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${LibraryVersion.RETROFIT}"
    const val RETROFIT_COROUTINES_ADAPTER = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${LibraryVersion.RETROFIT_COROUTINES_ADAPTER}"
    const val RETROFIT_GSON_CONVERTER = "com.squareup.retrofit2:converter-gson:${LibraryVersion.RETROFIT}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${LibraryVersion.COROUTINES}"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${LibraryVersion.COROUTINES}"
    const val LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${LibraryVersion.LOGGING_INTERCEPTOR}"
    const val KOIN = "io.insert-koin:koin-android:${LibraryVersion.KOIN}"
    const val VIEW_MODEL = "androidx.lifecycle:lifecycle-viewmodel-ktx:${LibraryVersion.VIEW_MODEL}"
    const val JUNIT = "junit:junit:${LibraryVersion.JUNIT}"
}

object LibraryVersion {
    const val KOTLIN = "1.5.0"
    const val CORE_KTX = "1.5.0"
    const val APP_COMPAT = "1.3.0"
    const val MATERIAL = "1.3.0"
    const val RETROFIT = "2.9.0"
    const val COROUTINES = "1.5.0"
    const val RETROFIT_COROUTINES_ADAPTER = "0.9.2"
    const val LOGGING_INTERCEPTOR = "4.9.1"
    const val KOIN = "3.0.1"
    const val VIEW_MODEL = "2.2.0"
    const val JUNIT = "4.12"
}