package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItemResponse
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test


class ProductsLocalCacheTest {
    private lateinit var objectUnderTest: ProductsLocalCache
    private lateinit var productItemResponseList: List<ProductItemResponse>

    @Before
    fun init() {
        objectUnderTest = ProductsLocalCache()
        productItemResponseList = listOf(
            ProductItemResponse("1", "", "", 10, 0F, "", ""),
            ProductItemResponse("2", "", "", 10, 0F, "", ""),
            ProductItemResponse("3", "", "", 10, 0F, "", ""),
            ProductItemResponse("4", "", "", 10, 0F, "", ""),
            ProductItemResponse("5", "", "", 10, 0F, "", ""),
        )
    }

    @Test
    fun `save products then get products`() {
        objectUnderTest.saveProducts(productItemResponseList)
        assertEquals(productItemResponseList.size, objectUnderTest.getProducts().size)
    }

    @Test
    fun `save products twice then get products`() {
        objectUnderTest.saveProducts(productItemResponseList)
        objectUnderTest.saveProducts(productItemResponseList)
        assertEquals(productItemResponseList.size, objectUnderTest.getProducts().size)
    }

    @Test
    fun `save product then update product assert object contains new product`() {
        val product = ProductItem("2", "", 10, "", "", 5)
        objectUnderTest.saveProducts(productItemResponseList)
        objectUnderTest.updateProduct(product)
        assertTrue(objectUnderTest.getProducts().contains(product))
    }

    @Test
    fun `save product then update product then get changedproducts`() {
        val productA = ProductItem("3", "", 8, "", "", 4)
        val productB = ProductItem("4", "", 9, "", "", 3)
        objectUnderTest.saveProducts(productItemResponseList)
        objectUnderTest.updateProduct(productA)
        objectUnderTest.updateProduct(productB)
        assertTrue(
            objectUnderTest.getChangedProducts()
                .contains(productA) && objectUnderTest.getChangedProducts().contains(productB)
        )
    }

    @Test
    fun `save product then update more than sold or available`() {
        val productWithChangedTitle = ProductItem("3", "", 8, "title", "", 4)
        val productWithoutChangedTitle = ProductItem("3", "", 8, "", "", 4)
        objectUnderTest.saveProducts(productItemResponseList)
        objectUnderTest.updateProduct(productWithChangedTitle)
        assertFalse(objectUnderTest.getProducts().contains(productWithChangedTitle))
        assertTrue(objectUnderTest.getProducts().contains(productWithoutChangedTitle))
    }
}