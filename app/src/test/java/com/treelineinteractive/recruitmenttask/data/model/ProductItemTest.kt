package com.treelineinteractive.recruitmenttask.data.model

import junit.framework.Assert.assertEquals
import org.junit.Test


class ProductItemTest {
    private lateinit var objectUnderTest: ProductItem

    @Test
    fun `calculate currentlyAvailable for initial state`() {
        val expectedAvailable = 10
        val sold = 0
        objectUnderTest = ProductItem("1", "", expectedAvailable, "", "", sold)
        assertEquals(expectedAvailable, objectUnderTest.currentlyAvailable)
    }

    @Test
    fun `calculate currentlyAvailable with sold greater than 0`() {
        val available = 10
        val sold = 7
        val expectedAvailable = 3

        objectUnderTest = ProductItem("1", "", available, "", "", sold)
        assertEquals(expectedAvailable, objectUnderTest.currentlyAvailable)
    }
}