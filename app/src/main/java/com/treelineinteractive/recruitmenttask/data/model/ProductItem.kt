package com.treelineinteractive.recruitmenttask.data.model

data class ProductItem(
    val id: String,
    val type: String,
    var available: Int,
    val title: String,
    val description: String,
    var sold: Int = 0,
) {
    val currentlyAvailable: Int
        get() = available - sold
}