package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItemResponse
import com.treelineinteractive.recruitmenttask.data.network.service.ShopService
import org.koin.dsl.module

val shopRepositoryModule = module {
    factory<ShopRepository> { ShopRepositoryImpl(get()) }
}

interface ShopRepository {
    suspend fun getProducts(): List<ProductItem>
    suspend fun getChangedProducts(): List<ProductItem>
    suspend fun updateProduct(productItem: ProductItem)
}

class ShopRepositoryImpl(
    private val shopService: ShopService
) : ShopRepository {
    private val productsLocalCache = ProductsLocalCache()

    override suspend fun getProducts(): List<ProductItem> {
        val products = shopService.getInventory()
        productsLocalCache.saveProducts(products)
        return productsLocalCache.getProducts()
    }

    override suspend fun getChangedProducts(): List<ProductItem> =
        productsLocalCache.getChangedProducts()

    override suspend fun updateProduct(productItem: ProductItem) {
        productsLocalCache.updateProduct(productItem)
    }
}

class ProductsLocalCache {
    private val savedProducts = mutableListOf<ProductItem>()

    fun saveProducts(productsResponse: List<ProductItemResponse>) {
        savedProducts.clear()
        savedProducts.addAll(productsResponse.map {
            ProductItem(
                it.id,
                it.type,
                it.available,
                it.title,
                it.description
            )
        }.toMutableList())
    }

    fun updateProduct(productItem: ProductItem) {
        savedProducts.find { productItem.id == it.id }.apply {
            this?.sold = productItem.sold
            this?.available = productItem.available
        }
    }

    fun getProducts() = savedProducts

    fun getChangedProducts() = savedProducts.filter { it.sold > 0 }
}