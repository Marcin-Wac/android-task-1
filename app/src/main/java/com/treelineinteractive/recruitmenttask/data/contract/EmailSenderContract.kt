package com.treelineinteractive.recruitmenttask.data.contract

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.activity.result.contract.ActivityResultContract
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.data.model.ProductItem
import com.treelineinteractive.recruitmenttask.utils.BOSS_EMAIL
import org.koin.dsl.module
import org.koin.java.KoinJavaComponent

val emailSenderModule = module {
    single { EmailSenderContract(get()) }
}

class EmailSenderContract(
    private val context: Context,
) : ActivityResultContract<String, Unit>() {

    fun prepareIntentWithReport(report: String): Intent? {
        return Intent.createChooser(
            createIntent(context, report),
            context.getText(R.string.chooser_send_report)
        )
    }

    override fun createIntent(context: Context, input: String?): Intent {
        return Intent(Intent.ACTION_SENDTO).also {
            it.data = Uri.parse("mailto:$BOSS_EMAIL")
            it.putExtra(Intent.EXTRA_SUBJECT, context.getText(R.string.report_email_title))
            it.putExtra(Intent.EXTRA_TEXT, input)
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?) {
    }
}

class ListParserForEmail {
    companion object {
        private val context: Context by KoinJavaComponent.inject(Context::class.java)
        fun parse(changedItems: List<ProductItem>): String {
            var report = context.getString(R.string.report_email_header)
            changedItems.forEach {
                report += String.format(
                    context.getString(R.string.report_email_content),
                    it.type,
                    it.title,
                    it.id,
                    it.sold,
                    it.currentlyAvailable
                )
            }
            report += context.getString(R.string.report_email_footer)
            return report
        }
    }
}