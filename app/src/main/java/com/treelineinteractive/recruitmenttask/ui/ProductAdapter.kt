package com.treelineinteractive.recruitmenttask.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.data.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding
import org.koin.java.KoinJavaComponent.inject

class ProductAdapter(
    private val changeValue: (ProductItem, Int) -> Unit,
) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
    private var dataset: List<ProductItem> = listOf()

    fun addList(productList: List<ProductItem>) {
        dataset = productList
        notifyItemRangeChanged(0, dataset.size)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataset[position]
        holder.bind(item, changeValue, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int = dataset.size

    class ViewHolder private constructor(private val binding: ViewProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context: Context by inject(Context::class.java)

        fun bind(item: ProductItem, changeValue: (ProductItem, Int) -> Unit, position: Int) {
            binding.nameLabel.text = item.title
            binding.descriptionLabel.text = item.description
            binding.statusLabel.text = String.format(
                context.getString(R.string.text_label_status),
                item.currentlyAvailable,
                item.sold
            )

            binding.incrementButton.isEnabled = item.currentlyAvailable > 0
            binding.decrementButton.isEnabled = item.sold > 0

            binding.incrementButton.setOnClickListener {
                if (item.currentlyAvailable > 0)
                    changeValue(item.apply { item.sold = item.sold + 1 }, position)
            }
            binding.decrementButton.setOnClickListener {
                if (item.sold > 0)
                    changeValue(item.apply { item.sold = item.sold - 1 }, position)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ViewProductItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}