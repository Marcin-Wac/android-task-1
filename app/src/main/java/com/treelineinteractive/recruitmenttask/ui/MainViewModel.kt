package com.treelineinteractive.recruitmenttask.ui

import androidx.lifecycle.viewModelScope
import com.treelineinteractive.recruitmenttask.data.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.utils.ToastNotificationManager
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject
import java.lang.Exception

class MainViewModel : BaseViewModel<MainViewModel.MainViewState, MainViewModel.MainViewAction>(MainViewState()) {
    private val shopRepository: ShopRepository by inject(ShopRepository::class.java)
    private val toastNotificationManager: ToastNotificationManager by inject(
        ToastNotificationManager::class.java
    )
    private var job: Job? = null

    data class MainViewState(
        val isLoading: Boolean = false,
        val error: String? = null,
        val items: List<ProductItem> = listOf(),
        val changedItems: List<ProductItem>? = null,
    ) : BaseViewState {
        val isSuccess: Boolean
            get() = !isLoading && error == null
    }

    sealed class MainViewAction : BaseAction {
        object LoadingProducts : MainViewAction()
        data class GetChangedProducts(val changedItems: List<ProductItem>) : MainViewAction()
        data class ProductsLoaded(val items: List<ProductItem>) : MainViewAction()
        data class ProductsLoadingError(val error: String) : MainViewAction()
    }

    fun loadProducts() {
        job?.cancel()
        job = viewModelScope.launch {
            sendAction(MainViewAction.LoadingProducts)
            try {
                sendAction(MainViewAction.ProductsLoaded(shopRepository.getProducts()))
            } catch (e: Exception) {
                sendAction(MainViewAction.ProductsLoadingError(e.message!!))
            }
        }
    }

    override fun onReduceState(viewAction: MainViewAction): MainViewState = when (viewAction) {
        is MainViewAction.LoadingProducts -> state.copy(isLoading = true, error = null)
        is MainViewAction.GetChangedProducts -> state.copy(
            isLoading = false,
            error = null,
            changedItems = viewAction.changedItems
        )
        is MainViewAction.ProductsLoaded -> state.copy(
            isLoading = false,
            error = null,
            items = viewAction.items
        )
        is MainViewAction.ProductsLoadingError -> state.copy(
            isLoading = false,
            error = viewAction.error
        )
    }

    fun changeValue(productItem: ProductItem) {
        viewModelScope.launch {
            shopRepository.updateProduct(productItem)
        }
    }

    fun sendReport() {
        job = viewModelScope.launch {
            shopRepository.getChangedProducts().apply {
                if (this.isEmpty())
                    toastNotificationManager.showToastReportDataEmpty()
                else
                    sendAction(MainViewAction.GetChangedProducts(this))
            }
        }
    }
}