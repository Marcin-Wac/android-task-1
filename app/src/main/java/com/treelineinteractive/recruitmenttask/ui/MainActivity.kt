package com.treelineinteractive.recruitmenttask.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.treelineinteractive.recruitmenttask.data.contract.EmailSenderContract
import com.treelineinteractive.recruitmenttask.data.contract.ListParserForEmail
import com.treelineinteractive.recruitmenttask.data.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val mainViewModel: MainViewModel by viewModel()
    private val emailSenderContract: EmailSenderContract by inject()
    private lateinit var listAdapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setAdapter()
        setOnClickListeners()
        mainViewModel.loadProducts()

        mainViewModel.stateLiveData.observe { state ->
            if (state.isSuccess)
                listAdapter.addList(state.items)

            state.changedItems?.let { sendReport(state.changedItems) }

            binding.sendReportButton.isVisible = state.isSuccess
            binding.itemsList.isVisible = state.isSuccess
            binding.progressBar.isVisible = state.isLoading
            binding.errorLayout.isVisible = state.error != null
            binding.errorLabel.text = state.error
        }
    }

    private fun setOnClickListeners() {
        binding.sendReportButton.setOnClickListener {
            mainViewModel.sendReport()
        }

        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }
    }

    private fun setAdapter() {
        listAdapter = ProductAdapter { product, pos ->
            mainViewModel.changeValue(productItem = product)
            listAdapter.notifyItemChanged(pos)
        }
        binding.itemsList.layoutManager = LinearLayoutManager(this)
        binding.itemsList.adapter = listAdapter
    }

    private fun sendReport(changedItems: List<ProductItem>) {
        startActivity(
            emailSenderContract.prepareIntentWithReport(
                ListParserForEmail.parse(
                    changedItems
                )
            )
        )
    }

    fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }
}