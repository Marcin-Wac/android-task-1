package com.treelineinteractive.recruitmenttask.utils

import android.content.Context
import android.widget.Toast
import com.treelineinteractive.recruitmenttask.R
import org.koin.dsl.module

val toastNotificationManagerModule = module {
    single { ToastNotificationManager(get()) }
}

class ToastNotificationManager(private val context: Context) {

    fun showToastReportDataEmpty() {
        Toast.makeText(
            context,
            context.getString(R.string.error_report_data_empty),
            Toast.LENGTH_SHORT
        ).show()
    }
}