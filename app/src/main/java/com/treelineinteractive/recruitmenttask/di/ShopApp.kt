package com.treelineinteractive.recruitmenttask.di

import android.app.Application
import com.treelineinteractive.recruitmenttask.data.contract.emailSenderModule
import com.treelineinteractive.recruitmenttask.data.repository.shopRepositoryModule
import com.treelineinteractive.recruitmenttask.utils.toastNotificationManagerModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ShopApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin(){
        startKoin {
            androidContext(this@ShopApp)
            modules(listOf(
                appModule,
                shopRepositoryModule,
                emailSenderModule,
                toastNotificationManagerModule,
            ))
        }
    }
}